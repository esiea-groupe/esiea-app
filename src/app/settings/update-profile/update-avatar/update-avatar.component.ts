/*
 * Copyright (c) 2023 DERACHE Adrien.
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import * as AuthActions from './../../../auth/store/auth.actions';
import { ToastService } from '../../../services/toast.service';
import * as fromApp from '../../../store/app.reducer';
import { Component, OnInit } from '@angular/core';
import { User } from '../../../models/user.model';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-update-avatar',
  templateUrl: './update-avatar.component.html',
  styleUrls: ['./update-avatar.component.scss'],
})
export class UpdateAvatarComponent implements OnInit {
  fileToSend: any | null = null;
  user: User;

  constructor(
    private store: Store<fromApp.AppState>,
    private toastService: ToastService
  ) {}

  ngOnInit() {
    this.store.select('auth').subscribe(async (authState) => {
      if (authState.errorMessage) {
        await this.toastService.presentToast(authState.errorMessage);
      }
      if (authState.user) {
        this.user = authState.user;
      }
    });
  }

  onFileSelected(event: any) {
    if (!event.target && !event.target.files[0]) {
      return;
    }
    this.fileToSend = event.target.files[0];
    const preview = URL.createObjectURL(this.fileToSend!);
    document.getElementById('imagePreview')?.setAttribute('src', preview);
  }

  onChangeAvatar() {
    if (this.fileToSend) {
      this.store.dispatch(
        new AuthActions.UpdateAvatarStart({
          avatar: this.fileToSend,
          user: this.user,
        })
      );
    }
  }
}
