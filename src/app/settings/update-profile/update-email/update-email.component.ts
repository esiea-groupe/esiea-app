import {FormControl, FormGroup, Validators} from '@angular/forms';
import * as AuthActions from './../../../auth/store/auth.actions';
import * as fromApp from '../../../store/app.reducer';
import {Component, OnInit} from '@angular/core';
import {ActionsSubject, Store} from '@ngrx/store';
import {Location} from '@angular/common';

@Component({
  selector: 'app-update-email',
  templateUrl: './update-email.component.html',
  styleUrls: ['./update-email.component.scss'],
})
export class UpdateEmailComponent implements OnInit {
  errorMessage: string | null;
  isLoading = true;

  emailForm: FormGroup = new FormGroup({
    'email': new FormControl(null, [Validators.email, Validators.required]),
    'password': new FormControl(null, Validators.required)
  })

  constructor(
    private store: Store<fromApp.AppState>,
    private actionsSubject: ActionsSubject,
    private location: Location) {
  }

  ngOnInit() {
    this.store.select('auth').subscribe(authState => {
      this.isLoading = authState.isLoading;
      this.errorMessage = authState.errorMessage;
      if (authState.user?.email === this.emailForm.value['email']) {
        this.emailForm.reset();
        this.location.back();
      }
    });
  }

  onChangeEmail() {
    if (this.emailForm.valid) {
      this.store.dispatch(new AuthActions.ChangeEmailStart({
          newEmail: this.emailForm.value['email'],
          password: this.emailForm.value['password']
        }
      ));
    }
  }
}
