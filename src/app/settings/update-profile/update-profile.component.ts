/*
 * Copyright (c) 2023 DERACHE Adrien.
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { SettingItem } from '../settings-list/settings-list.component';
import { Component } from '@angular/core';

@Component({
  selector: 'app-update-profile',
  templateUrl: './update-profile.component.html',
  styleUrls: ['./update-profile.component.scss'],
})
export class UpdateProfileComponent {
  settingItems: SettingItem[] = [
    {
      title: 'Modifier mon pseudo',
      description:
        'Consultez les informations relatives à votre compte, changez votre pseudo.',
      icon: 'person-outline',
      route: 'update-pseudo',
    },
    {
      title: 'Modifier mon avatar',
      description:
        "Modifiez votre avatar et plongez-vous dans l'univers des balles masquées.",
      icon: 'image-outline',
      route: 'update-avatar',
    },
    {
      title: 'Configuration de l’emploi du temps',
      description: "Changez les paramètres liée à l'emploi du temps.",
      icon: 'calendar-outline',
      route: 'schedule-preferences',
    },
    {
      title: 'Modifier mon email',
      description: 'Changez votre email.',
      icon: 'mail-outline',
      route: 'update-email',
    },
  ];
}
