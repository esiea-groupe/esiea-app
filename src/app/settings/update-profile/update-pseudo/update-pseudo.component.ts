import {FormControl, FormGroup, Validators} from '@angular/forms';
import * as AuthActions from './../../../auth/store/auth.actions';
import * as fromApp from '../../../store/app.reducer';
import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {Store} from '@ngrx/store';

@Component({
  selector: 'app-update-pseudo',
  templateUrl: './update-pseudo.component.html',
  styleUrls: ['./update-pseudo.component.scss'],
})
export class UpdatePseudoComponent implements OnInit {
  pseudoForm: FormGroup = new FormGroup(
    {
      'pseudo': new FormControl(null, [Validators.required, Validators.maxLength(50)])
    }
  )

  constructor(
    private store: Store<fromApp.AppState>,
    private location: Location
  ) {
  }

  ngOnInit() {
    this.store.select('auth').subscribe(authState => {
      if (authState.user?.name === this.pseudoForm.value['pseudo']) {
        this.location.back();
      }
    });
  }

  onChangePseudo() {
    if (this.pseudoForm.valid) {
      this.store.dispatch(new AuthActions.ChangePseudoStart({
          pseudo: this.pseudoForm.value['pseudo']
        }
      ));
    }
  }
}
