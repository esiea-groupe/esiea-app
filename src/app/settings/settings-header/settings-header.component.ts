import {Component, Input} from '@angular/core';
import {Location} from '@angular/common';

@Component({
  selector: 'app-settings-header',
  templateUrl: './settings-header.component.html',
  styleUrls: ['./settings-header.component.scss'],
})
export class SettingsHeaderComponent {
  @Input({required: true}) title: string;

  constructor(
    private _location: Location
  ) {
  }

  onBack() {
    this._location.back();
  }
}
