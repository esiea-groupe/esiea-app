/*
 * Copyright (c) 2023 DERACHE Adrien.
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { SettingItem } from './settings-list/settings-list.component';
import { AlertController, ModalController } from '@ionic/angular';
import * as AuthActions from './../auth/store/auth.actions';
import * as fromApp from '../store/app.reducer';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { User } from '../models/user.model';
import { ToastService } from '../services/toast.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent implements OnInit {
  user: User;
  settingItems: SettingItem[] = [
    {
      title: 'Modifier mon profil',
      description:
        'Consultez les informations relatives à votre compte, changez votre pseudo.',
      icon: 'person-outline',
      route: 'update-profile',
    },
    {
      title: 'Modifier mon mot de passe',
      description: "Changez votre mot de passe à n'importe quel moment.",
      icon: 'lock-closed-outline',
      route: 'update-password',
    },
    {
      title: 'Se déconnecter',
      description: 'Se déconnecter de cet appareil.',
      icon: 'log-out-outline',
      function: this.onLogOut,
    },
    {
      title: 'Supprimer mon compte',
      description:
        'Déconnecte tous les appareils et supprime toutes les données sur serveur vous concernant.',
      icon: 'trash-outline',
      function: this.onDeleteAccount,
    },
  ];

  constructor(
    private modalCtrl: ModalController,
    private store: Store<fromApp.AppState>,
    private alertController: AlertController,
    private toastService: ToastService
  ) {}

  ngOnInit() {
    this.store.select('auth').subscribe((authState) => {
      if (authState.user) {
        this.user = authState.user;
      }
      if (authState.errorMessage) {
        this.toastService.presentToast(authState.errorMessage);
      }
    });
  }

  callFunction(functionName: Function) {
    functionName.apply(this);
  }

  async onDeleteAccount() {
    const alert = await this.alertController.create({
      header: 'Confirmation',
      message: 'Voulez-vous vraiment vous supprimer votre compte ?',
      inputs: [
        {
          type: 'password',
          placeholder: 'Confirmez votre mot de passe',
          min: 1,
          name: 'password',
          max: 100,
        },
      ],
      buttons: [
        {
          text: 'Supprimer mon compte',
          role: 'confirm',
          cssClass: 'alert-button-confirm-danger',
          handler: (data) => {
            this.store.dispatch(
              new AuthActions.DeleteAccountStart({
                userId: this.user.$id,
                password: data.password,
              })
            );
          },
        },
        {
          text: 'Annuler',
          role: 'cancel',
        },
      ],
    });
    await alert.present();
  }

  async onLogOut() {
    const alert = await this.alertController.create({
      header: 'Confirmation',
      message: 'Voulez-vous vraiment vous déconnecter ?',
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
        },
        {
          text: 'Se déconnecter',
          role: 'confirm',
          cssClass: 'alert-button-confirm-danger',
          handler: () => {
            this.store.dispatch(new AuthActions.Logout());
          },
        },
      ],
    });
    await alert.present();
  }
}
