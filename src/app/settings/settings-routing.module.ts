/*
 * Copyright (c) 2023 DERACHE Adrien.
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { AppGuard } from '../auth/guards/app-guard.service';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { UpdatePseudoComponent } from './update-profile/update-pseudo/update-pseudo.component';
import { UpdateAvatarComponent } from './update-profile/update-avatar/update-avatar.component';
import { UpdateEmailComponent } from './update-profile/update-email/update-email.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { UpdateProfileComponent } from './update-profile/update-profile.component';
import { ScheduleComponent } from './update-profile/schedule/schedule.component';
import { SettingsComponent } from './settings.component';

const routes: Routes = [
  {
    path: '',
    canActivate: [AppGuard],
    children: [
      { path: '', component: SettingsComponent },
      { path: 'update-password', component: ChangePasswordComponent },
      {
        path: 'update-profile',
        children: [
          { path: '', component: UpdateProfileComponent },
          { path: 'update-email', component: UpdateEmailComponent },
          { path: 'update-pseudo', component: UpdatePseudoComponent },
          { path: 'schedule-preferences', component: ScheduleComponent },
          { path: 'update-avatar', component: UpdateAvatarComponent },
        ],
      },
    ],
  },
  {
    path: '',
    redirectTo: '/tabs/tab1',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class SettingsRoutingModule {}
