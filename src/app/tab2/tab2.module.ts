/*
 * Copyright (c) 2023 DERACHE Adrien.
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Tab2RoutingModule } from './tab2-routing.module';
import { registerLocaleData } from '@angular/common';
import { LOCALE_ID, NgModule } from '@angular/core';
import { ShareModule } from '../tabs/share.module';
import { NgCalendarModule } from 'ionic2-calendar';
import localeFr from '@angular/common/locales/fr';
import { Tab2Component } from './tab2.component';

registerLocaleData(localeFr, 'fr');

@NgModule({
  imports: [Tab2RoutingModule, ShareModule, NgCalendarModule],
  declarations: [Tab2Component],
  providers: [{ provide: LOCALE_ID, useValue: 'fr' }],
})
export class Tab2Module {}
