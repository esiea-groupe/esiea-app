/*
 * Copyright (c) 2023 DERACHE Adrien.
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Models } from 'appwrite';

export interface PollForm {
  options: string[];
  type: 'one' | 'multi';
}

export interface LikeRes extends Models.Document {
  users_id: string[];
}

export class EPouet {
  public id: string;
  public displayName: string;
  public numberLike: number;
  public isLiked: boolean;
  public userImgUrl: URL;
  public message: string;
  public warnPouet: string | null;
  public date: Date;
  public poll: PollForm | null;
  public pollRes: { [key: string]: number } | null;
  public isVoted: boolean;
  public imgUrl: URL | null;

  constructor(
    id: string,
    displayName: string,
    date: Date,
    message: string,
    warnPouet: string | null,
    userImgUrl: URL,
    numberLike: number,
    isLiked: boolean,
    poll: PollForm | null,
    pollRes: { [key: string]: number } | null,
    isVoted: boolean,
    imgUrl: URL | null
  ) {
    this.id = id;
    this.numberLike = numberLike;
    this.isLiked = isLiked;
    this.displayName = displayName;
    this.userImgUrl = userImgUrl;
    this.message = message;
    this.warnPouet = warnPouet;
    this.date = date;
    this.poll = poll;
    this.pollRes = pollRes;
    this.isVoted = isVoted;
    this.imgUrl = imgUrl;
  }
}
