import * as AuthActions from './auth/store/auth.actions';
import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {
  constructor(private store: Store) {
  }

  ngOnInit() {
    this.store.dispatch(new AuthActions.AutoLogin());
  }
}
