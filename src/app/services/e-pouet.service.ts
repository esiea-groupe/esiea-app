/*
 * Copyright (c) 2023 DERACHE Adrien.
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { EPouet, LikeRes, PollForm } from '../models/e-pouet.model';
import { environment } from '../../environments/environment';
import * as fromApp from '../store/app.reducer';
import { Injectable } from '@angular/core';
import { Models, Query } from 'appwrite';
import { BehaviorSubject } from 'rxjs';
import { Api } from '../helpers/api';
import { Store } from '@ngrx/store';

@Injectable({ providedIn: 'root' })
export class EPouetService {
  realTimePouet = new BehaviorSubject<EPouet[] | null>(null);
  userId: string;

  constructor(private store: Store<fromApp.AppState>) {
    Api.client().subscribe(
      [
        `databases.${environment.socialDatabase}.collections.${environment.ePouetCollection}.documents`,
        `databases.${environment.socialDatabase}.collections.${environment.likesCollection}.documents`,
        `databases.${environment.socialDatabase}.collections.${environment.voteCollection}.documents`,
      ],
      async () => {
        this.realTimePouet.next(await this.getPouets());
      }
    );
    this.store.select('auth').subscribe((authState) => {
      if (authState.user) {
        this.userId = authState.user?.$id;
      }
    });
  }

  /**
   * Add/Remove a like to pouet.
   * @param pouetId The pouet where to add/remove a like.
   */
  likePouet(pouetId: string) {
    return Api.functions().createExecution(
      environment.addLikeFunction,
      JSON.stringify({ pouetId })
    );
  }

  /**
   * Fetch e-pouets from Appwrite.
   */
  async getPouets(): Promise<EPouet[]> {
    let ePouets: EPouet[] = [];
    const res = await Api.databases().listDocuments(
      environment.socialDatabase,
      environment.ePouetCollection,
      [Query.orderDesc('$createdAt')]
    );
    const likes: Models.DocumentList<LikeRes> =
      await Api.databases().listDocuments<LikeRes>(
        environment.socialDatabase,
        environment.likesCollection
      );
    const votes = await Api.databases().listDocuments(
      environment.socialDatabase,
      environment.voteCollection
    );
    const avatars = await Api.storages().listFiles(environment.bucketId);
    res.documents.forEach((document: Models.Document) => {
      // Likes
      let isLiked = false;
      let numberLike = 0;
      likes.documents.forEach((like: LikeRes) => {
        if (like.$id === document.$id) {
          if (like.users_id.includes(this.userId)) {
            isLiked = true;
          }
          numberLike = like.users_id.length;
          return;
        }
      });

      // Avatars
      let userImgUrl: URL = Api.avatars().getInitials(
        document['user_name'],
        48,
        48
      );
      avatars.files.forEach((file) => {
        if (file.$id === document['user_id']) {
          userImgUrl = new URL(
            Api.storages().getFilePreview(
              environment.bucketId,
              document['user_id'],
              48,
              48,
              undefined,
              100
            ).href +
              '&random=' +
              Math.random()
          );
          return;
        }
      });

      // Votes
      let isVoted = false;
      let pollRes: { [key: string]: number } | null = null;
      votes.documents.forEach((vote, index) => {
        if (vote.$id === document.$id) {
          pollRes = JSON.parse(votes.documents[index]['value']);
          isVoted = votes.documents[index]['users'].includes(this.userId);
        }
      });

      // Push pouet
      ePouets.push(
        new EPouet(
          document.$id,
          document['user_name'],
          new Date(document.$createdAt),
          document['message'],
          document['warning_message'],
          userImgUrl,
          numberLike,
          isLiked,
          JSON.parse(document['poll']),
          pollRes,
          isVoted,
          document['images_id'].length > 0
            ? Api.storages().getFilePreview(
                environment.pouetBucket,
                document['images_id']
              )
            : null
        )
      );
    });
    return ePouets;
  }

  /**
   * Send a poll vote to Appwrite.
   *
   * @param {string} pouetId - The pouet id of the corresponding poll.
   * @param {string|string[]} value - Value the value property
   */
  sendVote(pouetId: string, value: string | string[]) {
    return Api.functions().createExecution(
      environment.voteFunction,
      JSON.stringify({ pouetId, value })
    );
  }

  /**
   * Sends a 'Pouet' message along with optional poll information to Appwrite.
   *
   * @param {string} userId - The ID of the user sending the message.
   * @param {string} name - The name of the user sending the message.
   * @param {string} message - The main content of the 'Pouet' message.
   * @param {string} warningMessage - A warning or additional information related to the message.
   * @param {PollForm | null} poll - Optional poll information attached to the message (can be null if no poll).
   * @param image
   * @returns {Promise<Object>} - A promise that resolves to the newly created document in the database.
   *
   * @throws {Error} If the creation of the document fails or encounters an error.
   */
  async sendPouet(
    userId: string,
    name: string,
    message: string,
    warningMessage: string,
    poll: PollForm | null,
    image: File | null
  ) {
    let imageId = null;
    if (image) {
      imageId = await Api.storages().createFile(
        environment.pouetBucket,
        'unique()',
        image
      );
    }
    return Api.databases().createDocument(
      environment.socialDatabase,
      environment.ePouetCollection,
      'unique()',
      {
        user_id: userId,
        user_name: name,
        message,
        warning_message: warningMessage,
        poll: JSON.stringify(poll),
        images_id: imageId ? [imageId.$id] : null,
      }
    );
  }
}
