/*
 * Copyright (c) 2023 DERACHE Adrien.
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { EPouetService } from '../../../services/e-pouet.service';
import { ToastService } from '../../../services/toast.service';
import { EPouet } from '../../../models/e-pouet.model';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-e-pouet-item',
  templateUrl: './e-pouet-item.component.html',
  styleUrls: ['./e-pouet-item.component.scss'],
})
export class EPouetItemComponent {
  @Input({ required: true }) ePouet: EPouet;
  isAvatarLoading = true;
  isVoteSending = false;
  radioValue: string;
  checkboxSelected: boolean[] = [];

  constructor(
    private ePouetService: EPouetService,
    private toastService: ToastService
  ) {}

  getTime(time: Date): string {
    const currentDate = new Date();

    if (
      currentDate.getDay() !== time.getDay() ||
      currentDate.getMonth() - time.getMonth() > 0 ||
      currentDate.getFullYear() - time.getFullYear() > 0
    ) {
      return time.toDateString();
    } else if (currentDate.getHours() - time.getHours() > 0) {
      return String(currentDate.getHours() - time.getHours()) + 'h';
    } else if (currentDate.getMinutes() - time.getMinutes() > 0) {
      return String(currentDate.getMinutes() - time.getMinutes()) + 'min';
    } else {
      return String(currentDate.getSeconds() - time.getSeconds()) + 'sec';
    }
  }

  onAvatarLoaded() {
    this.isAvatarLoading = false;
  }

  isValid() {
    if (this.isVoteSending) {
      return false;
    }
    return !!(
      this.radioValue || this.checkboxSelected.filter((x) => x).length > 0
    );
  }

  onLike() {
    this.ePouetService.likePouet(this.ePouet.id).then(
      () => {},
      async () => {
        await this.toastService.presentToast("Une erreur c'est produite.");
      }
    );
  }

  onVote() {
    if (
      (!this.radioValue && this.checkboxSelected.length === 0) ||
      this.isVoteSending
    ) {
      return;
    }
    this.isVoteSending = true;
    if (this.ePouet.poll?.type === 'one') {
      this.ePouetService
        .sendVote(this.ePouet.id, this.radioValue)
        .then((res) => {
          this.isVoteSending = false;
          console.log(res);
        });
    } else {
      const optionSelected = [];
      for (let i = 0; i < this.checkboxSelected.length; i++) {
        if (this.checkboxSelected[i]) {
          optionSelected.push(this.ePouet.poll!.options[i]);
        }
      }
      this.ePouetService
        .sendVote(this.ePouet.id, optionSelected)
        .then((res) => {
          console.log(res);
          this.isVoteSending = false;
        });
    }
  }

  isMax(option: string) {
    if (!this.ePouet.pollRes) {
      return false;
    }
    const value = this.ePouet.pollRes[option] ?? 0;

    for (const key of Object.keys(this.ePouet.pollRes)) {
      if (this.ePouet.pollRes[key] > value) {
        return false;
      }
    }
    return true;
  }

  setBackground(option: string) {
    return {
      '--background': `linear-gradient(90deg, var(--ion-color-secondary-tint) ${this.getVotePourcentage(
        option
      )}%, rgba(255, 255, 255, 0) 0%)`,
    };
  }

  getVotePourcentage(option: string) {
    let totalRes = 0;
    Object.keys(this.ePouet.pollRes!).forEach((key) => {
      totalRes += this.ePouet.pollRes![key];
    });
    const countCurrentOption = this.ePouet.pollRes![option]
      ? this.ePouet.pollRes![option]
      : 0;
    return Math.round((countCurrentOption * 100) / totalRes);
  }
}
