import { ModalController } from '@ionic/angular';
import { Component } from '@angular/core';
import { EPouetWriterComponent } from './e-pouet-writer/e-pouet-writer.component';

@Component({
  selector: 'app-tab1',
  templateUrl: './tab1.component.html',
  styleUrls: ['./tab1.component.scss'],
})
export class Tab1Component {
  constructor(private modalCtrl: ModalController) {}

  async onEPouet() {
    const modal = await this.modalCtrl.create({
      component: EPouetWriterComponent,
    });
    await modal.present();
  }
}
