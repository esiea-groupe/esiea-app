/*
 * Copyright (c) 2023 DERACHE Adrien.
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { AlertController, ItemReorderEventDetail, ModalController } from '@ionic/angular';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { EPouetService } from '../../services/e-pouet.service';
import { PollForm } from '../../models/e-pouet.model';
import * as fromApp from '../../store/app.reducer';
import { Component, OnInit } from '@angular/core';
import { User } from '../../models/user.model';
import { Store } from '@ngrx/store';

interface PouetForm {
  pouet: FormControl<string>;
  pouetWarn: FormControl<string>;
  poll: FormArray<FormControl<string>>;
  pollType: FormControl<'one' | 'multi'>;
}

@Component({
  selector: 'app-e-pouet-writer',
  templateUrl: './e-pouet-writer.component.html',
  styleUrls: ['./e-pouet-writer.component.scss'],
})
export class EPouetWriterComponent implements OnInit {
  user: User;
  fileToSend: any | null = null;
  pouetForm: FormGroup<PouetForm> = new FormGroup<PouetForm>({
    pouet: new FormControl<string>('', {
      nonNullable: true,
      validators: [Validators.required, Validators.maxLength(300)],
    }),
    pouetWarn: new FormControl<string>('', {
      nonNullable: true,
      validators: Validators.maxLength(50),
    }),
    poll: new FormArray([
      new FormControl<string>('', {
        nonNullable: true,
        validators: Validators.maxLength(30),
      }),
      new FormControl<string>('', {
        nonNullable: true,
        validators: Validators.maxLength(30),
      }),
    ]),
    pollType: new FormControl<'one' | 'multi'>('one', { nonNullable: true }),
  });
  warnPouet = false;
  pollPouet = false;
  lockPouet = false;
  photo = false;
  protected readonly String = String;

  constructor(
    private modalCtrl: ModalController,
    private store: Store<fromApp.AppState>,
    private pouetService: EPouetService,
    private alertController: AlertController
  ) {}

  get controls() {
    return (this.pouetForm.get('poll') as FormArray).controls;
  }

  ngOnInit() {
    this.store.select('auth').subscribe((authState) => {
      if (authState.user) {
        this.user = authState.user;
      }
    });
  }

  onEPouet() {
    console.log(this.pouetForm.value);
    if (this.user.$id && this.pouetForm.valid) {
      this.lockPouet = true;
      let poll: PollForm | null = null;
      if (this.pollPouet && this.pouetForm.value.pollType) {
        poll = { options: [], type: this.pouetForm.value.pollType };
        for (let option of this.pouetForm.value.poll!) {
          poll.options.push(option);
        }
      }
      this.pouetService
        .sendPouet(
          this.user.$id,
          this.user.name,
          this.pouetForm.value.pouet!,
          this.pouetForm.value.pouetWarn!,
          poll,
          this.fileToSend
        )
        .then(
          () => {
            this.dismissModal();
          },
          () => {
            this.lockPouet = false;
          }
        );
    }
  }

  onWarnPouet() {
    this.warnPouet = !this.warnPouet;
  }

  onPollPouet() {
    this.pollPouet = !this.pollPouet;
  }

  dismissModal() {
    this.modalCtrl.dismiss().then();
  }

  addOption() {
    const formArray = <FormArray>this.pouetForm.get('poll');
    const control = new FormControl('', {
      nonNullable: true,
      validators: Validators.maxLength(30),
    });
    if (formArray.length < 4) {
      formArray.push(control);
    }
  }

  async formType() {
    const alert = await this.alertController.create({
      header: 'Style de sondage',
      inputs: [
        {
          type: 'radio',
          label: 'Une seule option',
          value: 'one',
          checked: true,
        },
        {
          type: 'radio',
          value: 'multi',
          label: 'Plusieurs options',
        },
      ],
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
        },
        {
          text: 'Ok',
          role: 'confirm',
          handler: (type) => {
            this.pouetForm.get('pollType')?.setValue(type);
          },
        },
      ],
    });

    await alert.present();
  }

  onFileSelected(event: any) {
    this.photo = true;
    this.pollPouet = false;
    if (!event.target && !event.target.files[0]) {
      return;
    }
    this.fileToSend = event.target.files[0];
    const preview = URL.createObjectURL(this.fileToSend!);
    document.getElementById('imagePreview')?.setAttribute('src', preview);
  }

  removeOption() {
    const formArray = <FormArray>this.pouetForm.get('poll');
    if (formArray.length > 2) {
      (<FormArray>this.pouetForm.get('poll')).removeAt(-1);
    }
  }

  handleReorder(ev: CustomEvent<ItemReorderEventDetail>) {
    ev.detail.complete();
  }
}
