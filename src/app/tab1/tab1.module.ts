/*
 * Copyright (c) 2023 DERACHE Adrien.
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Tab1RoutingModule } from './tab1-routing.module';
import { ShareModule } from '../tabs/share.module';
import { NgModule } from '@angular/core';

import { EPouetItemComponent } from './e-pouet-list/e-pouet-item/e-pouet-item.component';
import { EPouetWriterComponent } from './e-pouet-writer/e-pouet-writer.component';
import { EPouetListComponent } from './e-pouet-list/e-pouet-list.component';
import { Tab1Component } from './tab1.component';
import { NgOptimizedImage } from '@angular/common';

@NgModule({
  imports: [
    Tab1RoutingModule,
    ShareModule,
    ReactiveFormsModule,
    FormsModule,
    NgOptimizedImage,
  ],
  declarations: [
    Tab1Component,
    EPouetItemComponent,
    EPouetListComponent,
    EPouetWriterComponent,
  ],
})
export class Tab1Module {}
