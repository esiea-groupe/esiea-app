import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {Tab1Component} from './tab1.component';


const routes: Routes = [
  {path: '', component: Tab1Component}
]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class Tab1RoutingModule {
}
