import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss'],
})
export class TabsComponent implements OnInit {
  tabSelected: string = '/tabs/tab1';

  constructor(private router: Router) {
  }

  ngOnInit() {
    this.router.events.subscribe(() => {
      this.tabSelected = this.router.url;
    });
  }
}
