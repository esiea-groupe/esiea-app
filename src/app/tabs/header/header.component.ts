/*
 * Copyright (c) 2023 DERACHE Adrien.
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ScheduleService } from '../../services/schedule.service';
import * as fromApp from '../../store/app.reducer';
import { User } from '../../models/user.model';
import { Subscription } from 'rxjs';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit, OnDestroy {
  @Input() mode: 'calendar' | 'normal' = 'normal';
  isDarkModeEnable = window.matchMedia('(prefers-color-scheme: dark)').matches;
  weekday: { weekday: string; date: Date }[] = [];
  minDate = new Date().getFullYear() - 1;
  maxDate = new Date().getFullYear() + 1;
  selectedDate: Date = new Date();
  dateSubscription: Subscription;
  user: User;

  constructor(
    private scheduleService: ScheduleService,
    private store: Store<fromApp.AppState>
  ) {}

  isWeekday = (dateString: string) => {
    const date = new Date(dateString);
    const utcDay = date.getUTCDay();

    return utcDay !== 0 && utcDay !== 6;
  };

  ngOnInit() {
    if (this.mode == 'calendar') {
      this.dateSubscription = this.scheduleService.dateSelected.subscribe(
        (date) => {
          this.selectedDate = date;
          this.updateWeekDay(date);
        }
      );
    }
    this.store.select('auth').subscribe((authState) => {
      if (authState.user) {
        this.user = authState.user;
      }
    });
  }

  updateWeekDay(date: Date) {
    const dayRange = 5;
    const weekdayList = [];
    for (let i = 0; i < dayRange; i++) {
      const targetDate = new Date(
        date.getFullYear(),
        date.getMonth(),
        date.getDate() - date.getDay() + 1 + i,
        4 // To avoid issue with ion-datetime
      );
      const weekday = targetDate.toLocaleString('fr-FR', {
        weekday: 'short',
      });
      weekdayList.push({ weekday: weekday, date: targetDate });
    }
    this.weekday = weekdayList;
  }

  onChangeDate(event: any) {
    if (event) {
      this.scheduleService.dateSelected.next(new Date(event));
    }
  }

  ngOnDestroy() {
    this.dateSubscription.unsubscribe();
  }
}
