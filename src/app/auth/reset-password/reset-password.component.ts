import {
  AbstractControl,
  FormControl,
  FormGroup,
  ValidationErrors,
  Validators,
} from '@angular/forms';
import { ActivatedRoute, Route, Router } from '@angular/router';
import * as AuthActions from './../store/auth.actions';
import * as fromApp from '../../store/app.reducer';
import { Component, OnInit } from '@angular/core';
import { ActionsSubject, Store } from '@ngrx/store';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss'],
})
export class ResetPasswordComponent implements OnInit {
  isLoading = true;
  messageError: string | null = null;
  isFromLink = false;
  passwordRecoveryForm: FormGroup = new FormGroup({
    email: new FormControl(null, [Validators.email, Validators.required]),
  });
  passwordResetForm: FormGroup = new FormGroup({
    password: new FormControl(null, [
      Validators.required,
      Validators.minLength(12),
    ]),
    passwordConfirmation: new FormControl(null, [
      Validators.required,
      this.isSamePassword.bind(this),
    ]),
  });
  constructor(
    private store: Store<fromApp.AppState>,
    private actionsSubject: ActionsSubject,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.isFromLink = !!this.route.snapshot.queryParams['userId'];
    this.store.select('auth').subscribe((authState) => {
      this.isLoading = authState.isLoading;
      this.messageError = authState.errorMessage;
    });
    this.actionsSubject.subscribe((action) => {
      if (action.type === '[Auth] Recover Password') {
        this.router.navigate(['login']);
      }
    });
  }

  isSamePassword(control: AbstractControl): ValidationErrors | null {
    if (control.value && this.passwordResetForm) {
      if (control.value === this.passwordResetForm.value['password']) {
        return null;
      }
    }
    return { notSamePasswords: true };
  }

  onReset() {
    if (this.passwordResetForm.valid) {
      this.store.dispatch(
        new AuthActions.RecoverPasswordCheck({
          userId: this.route.snapshot.queryParams['userId'],
          secret: this.route.snapshot.queryParams['secret'],
          password: this.passwordResetForm.value.password,
          passwordConfirmation:
            this.passwordResetForm.value.passwordConfirmation,
        })
      );
    }
  }
  onSendLink() {
    if (this.passwordRecoveryForm.valid) {
      this.store.dispatch(
        new AuthActions.RecoverPasswordStart({
          email: this.passwordRecoveryForm.value.email,
        })
      );
    }
  }
}
