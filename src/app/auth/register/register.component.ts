import {FormControl, FormGroup, Validators} from '@angular/forms';
import * as AuthActions from './../store/auth.actions';
import * as fromApp from '../../store/app.reducer';
import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  isLoading = true;
  messageError: string | null = null;
  registerForm: FormGroup = new FormGroup(
    {
      'pseudo': new FormControl(null, Validators.required),
      'email': new FormControl(null, [Validators.email, Validators.required]),
      'password': new FormControl(null, [Validators.required, Validators.minLength(12)])
    }
  );

  constructor(private store: Store<fromApp.AppState>) {
  }

  ngOnInit() {
    this.store.select('auth').subscribe(authState => {
      this.isLoading = authState.isLoading;
      this.messageError = authState.errorMessage;
      if (authState.errorMessage === null) {
        this.messageError = null;
        return;
      }
      this.messageError = authState.errorMessage;
    });
  }

  onRegister() {
    if (!this.registerForm.valid) {
      return;
    }
    this.store.dispatch(new AuthActions.RegisterStart({
        pseudo: this.registerForm.value.pseudo,
        email: this.registerForm.value.email,
        password: this.registerForm.value.password
      }
    ));
  }

}
