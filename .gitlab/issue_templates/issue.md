### Short description of the problem


### Steps to reproduce
1. 
2. 
3. 

### Expected behaviour
- Tell us what should happen

### Actual behaviour
- Tell us what happens


### Environment data
Android version:

Device model: 

Stock or customized system:

ESIEA app version:


### Logs
#### App error message
```
Insert your message error here
```
