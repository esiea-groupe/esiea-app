/*
 * Copyright (c) 2023 DERACHE Adrien.
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const sdk = require("node-appwrite");

/*
  'req' variable has:
    'headers' - object with request headers
    'payload' - request body data as a string
    'variables' - object with function variables

  'res' variable has:
    'send(text, status)' - function to return text response. Status code defaults to 200
    'json(obj, status)' - function to return JSON response. Status code defaults to 200

  If an error is thrown, a response with code 500 will be returned.
*/

module.exports = async function (req, res) {
  const client = new sdk.Client();
  const databases = new sdk.Databases(client);
  const payload = JSON.parse(req.payload ?? "{}");

  const pouetId = payload.pouetId;
  const userId = req.variables["APPWRITE_FUNCTION_USER_ID"];
  const socialDatabaseId = "649a9c89b945e287a092";
  const likeCollectionId = "649a9eeb712ced5b5d9f";

  if (pouetId === undefined || pouetId === null) {
    res.json(
      {
        message: "Pouet ID is missing.",
        code: "400",
        type: "missing_PouetID",
      },
      400
    );
  }

  if (
    !req.variables["APPWRITE_FUNCTION_ENDPOINT"] ||
    !req.variables["APPWRITE_FUNCTION_API_KEY"]
  ) {
    console.warn(
      "Environment variables are not set. Function cannot use Appwrite SDK."
    );
    res.json(
      {
        message: "Environment variables are not set.",
        code: "500",
        type: "environment_variables",
      },
      500
    );
  } else {
    client
      .setEndpoint(req.variables["APPWRITE_FUNCTION_ENDPOINT"])
      .setProject(req.variables["APPWRITE_FUNCTION_PROJECT_ID"])
      .setKey(req.variables["APPWRITE_FUNCTION_API_KEY"]);
  }

  databases.getDocument(socialDatabaseId, likeCollectionId, pouetId).then(
    (document) => {
      if (!document["users_id"].includes(userId)) {
        // Add a like
        databases.updateDocument(socialDatabaseId, likeCollectionId, pouetId, {
          users_id: [...document["users_id"], userId],
        });
        return res.send("Ok", 200);
      } else {
        // Remove a like
        databases.updateDocument(socialDatabaseId, likeCollectionId, pouetId, {
          users_id: document["users_id"].filter((user) => user !== userId),
        });
        return res.send("Ok", 200);
      }
    },
    () => {
      // Create the document
      databases
        .createDocument(socialDatabaseId, likeCollectionId, pouetId, {
          users_id: [...[], userId],
        })
        .then(
          () => {
            return res.send("Ok", 200);
          },
          () => {
            res.json(
              {
                message: "An unknown error has occurred.",
                code: "500",
                type: "general_unknown",
              },
              500
            );
          }
        );
    }
  );
};
