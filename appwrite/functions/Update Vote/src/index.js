/*
 * Copyright (c) 2023 DERACHE Adrien.
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const sdk = require("node-appwrite");

/*
  'req' variable has:
    'headers' - object with request headers
    'payload' - request body data as a string
    'variables' - object with function variables

  'res' variable has:
    'send(text, status)' - function to return text response. Status code defaults to 200
    'json(obj, status)' - function to return JSON response. Status code defaults to 200

  If an error is thrown, a response with code 500 will be returned.
*/

module.exports = async function (req, res) {
  const client = new sdk.Client();
  const databases = new sdk.Databases(client);
  const payload = JSON.parse(req.payload ?? "{}");

  const pouetId = payload.pouetId;
  const userId = req.variables["APPWRITE_FUNCTION_USER_ID"];
  const voteData = payload.value;
  const socialDatabaseId = "649a9c89b945e287a092";
  const voteCollectionId = "64a8235f76db5003691f";

  if (pouetId === undefined || pouetId === null) {
    res.json(
      {
        message: "Pouet ID is missing.",
        code: "400",
        type: "missing_PouetID",
      },
      400
    );
  }

  if (voteData === undefined || voteData === null) {
    res.json(
      {
        message: "Vote is missing.",
        code: "400",
        type: "missing_vote",
      },
      400
    );
  }

  if (
    !req.variables["APPWRITE_FUNCTION_ENDPOINT"] ||
    !req.variables["APPWRITE_FUNCTION_API_KEY"]
  ) {
    console.warn(
      "Environment variables are not set. Function cannot use Appwrite SDK."
    );
    res.json(
      {
        message: "Environment variables are not set.",
        code: "500",
        type: "environment_variables",
      },
      500
    );
  } else {
    client
      .setEndpoint(req.variables["APPWRITE_FUNCTION_ENDPOINT"])
      .setProject(req.variables["APPWRITE_FUNCTION_PROJECT_ID"])
      .setKey(req.variables["APPWRITE_FUNCTION_API_KEY"]);
  }

  databases
    .listDocuments(socialDatabaseId, voteCollectionId, [
      sdk.Query.equal("$id", pouetId),
    ])
    .then(
      (votes) => {
        if (votes.total === 1) {
          // Add a vote
          databases
            .getDocument(socialDatabaseId, voteCollectionId, pouetId)
            .then(
              (document) => {
                if (!document["users"].includes(userId)) {
                  const votes = JSON.parse(document["value"]);
                  databases.updateDocument(
                    socialDatabaseId,
                    voteCollectionId,
                    pouetId,
                    {
                      users: [...document["users"], userId],
                      value: JSON.stringify(buildPollRes(voteData, votes)),
                    }
                  );
                  return res.send(JSON.stringify(obj), 200);
                } else {
                  // Already voted
                  return res.send("Already voted", 200);
                }
              },
              () =>
                res.json(
                  {
                    message: "An unknown error has occurred.",
                    code: "500",
                    type: "general_unknown",
                  },
                  500
                )
            );
        } else {
          // Create the document
          databases
            .createDocument(socialDatabaseId, voteCollectionId, pouetId, {
              users: [...[], userId],
              value: JSON.stringify(buildPollRes(voteData)),
            })
            .then(
              () => {
                return res.send("Ok", 200);
              },
              () => {
                res.json(
                  {
                    message: "An unknown error has occurred.",
                    code: "500",
                    type: "general_unknown",
                  },
                  500
                );
              }
            );
        }
      },
      () => {
        res.json(
          {
            message: "An unknown error has occurred.",
            code: "500",
            type: "general_unknown",
          },
          500
        );
      }
    );
};

function buildPollRes(options, currentValue = {}) {
  const obj = currentValue;
  if (Array.isArray(options)) {
    for (let option of options) {
      obj[option] = obj[option] ? obj[option] + 1 : 1;
    }
  } else {
    obj[options] = obj[options] ? obj[options] + 1 : 1;
  }
  return obj;
}
