# ESIEA Assos

<p align="center">
  <a href="https://gitlab.com/esiea-groupe/esiea-app/-/blob/master/LICENSE">
    <img src="https://img.shields.io/badge/license-GPLv3-blue.svg?style=flat-square" alt="ESIEA Assos is released under the GPLv3 license." />
  </a>
</p>

<a href="https://play.google.com/store/apps/details?id=fr.bugprogEnterprise.mygarden"> 
  <img src="https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png"  width="200">
</a>

**Unofficial ESIEA Assos**

<img src="./doc/esiea_screenshot.png" alt="drawing" width="200"/>

ESIEA Assos is built with Web Technologies:  
[![Typescript](https://img.shields.io/badge/Typescript-3178c6?style=for-the-badge&logo=typescript&labelColor=gray)](https://www.typescriptlang.org/)
[![Angular](https://img.shields.io/badge/Angular-DD0031?style=for-the-badge&logo=angular&labelColor=gray)](https://angular.io/)
[![Ionic](https://img.shields.io/badge/IONIC-66b2ff?style=for-the-badge&logo=ionic&labelColor=gray&logoColor=66b2ff)](https://ionicframework.com/)

<!-- TOC -->
* [ESIEA Assos](#esiea-assos)
  * [How to contribute 🚀](#how-to-contribute-)
  * [Development](#development)
      * [Download Required Tools](#download-required-tools)
      * [Install Node.js/npm](#install-nodejsnpm)
      * [Install the Ionic CLI](#install-the-ionic-cli)
    * [Get ESIEA files](#get-esiea-files)
    * [Run the App](#run-the-app)
      * [Build for Android](#build-for-android)
  * [Appwrite Structure](#appwrite-structure)
    * [Auth](#auth)
  * [Databases](#databases)
    * [e-pouet Collection](#e-pouet-collection)
        * [Permissions](#permissions)
        * [Attributes](#attributes)
    * [likes Collection](#likes-collection)
        * [Permissions](#permissions-1)
        * [Attributes](#attributes-1)
    * [votes Collection](#votes-collection)
        * [Permissions](#permissions-2)
        * [Attributes](#attributes-2)
  * [Functions](#functions)
    * [Likes](#likes)
      * [Payload](#payload)
    * [Update Vote](#update-vote)
      * [Payload](#payload-1)
    * [Delete Account](#delete-account)
      * [Payload](#payload-2)
  * [Storage](#storage)
    * [Avatars Bucket](#avatars-bucket)
    * [E-Pouet](#e-pouet)
  * [Support](#support)
  * [Licence](#licence)
<!-- TOC -->

## How to contribute 🚀

Thank you for your contribution to the open source world !😍

- our forum at [Discord](https://discord.gg/CTVrrCa2YQ) or [Matrix](https://matrix.to/#/#MyGarden:matrix.org)
- opening [issues](https://gitlab.com/esiea-groupe/esiea-app/-/issues/new)

## Development

ESIEA Assos is an app build with Ionic, Angular and Capacitor.
#### Download Required Tools

Download and install these right away to ensure an optimal Ionic development experience:

- Node.js for interacting with the Ionic ecosystem. Download the LTS version here.
- A code editor for... writing code! We are fans of Visual Studio Code.
- Command-line interface/terminal (CLI):
    - Windows users: for the best Ionic experience, we recommend the built-in command line (cmd) or the Powershell CLI, running in Administrator mode.
    - Mac/Linux users, virtually any terminal will work.
      
#### Install Node.js/npm

```shell
sudo apt install nodejs npm
```

To make sure that npm has been installed successfully, run the following command:

```
npm --version
8.5.0
```

#### Install the Ionic CLI
Install the Ionic CLI with npm:
```shell
sudo npm install -g @ionic/cli
```

### Get ESIEA files

Clone the [repository](https://gitlab.com/esiea-groupe/esiea-app):

git clone https://gitlab.com/esiea-groupe/esiea-app.git

cd esiea/

### Run the App

Install the dependency:
```shell
npm install
```

Run this command next:

```shell
ionic serve
```

Your Ionic app is now running in a web browser at localhost:8100. Most of your app can be built and tested right in the browser, greatly increasing development and testing speed.

#### Build for Android

Follow this [guide](https://capacitorjs.com/docs/android).

## Appwrite Structure

[Appwrite](https://appwrite.io/) is a powerful open-source backend solution.
The entire app is build with an Appwrite background.

### Auth
There are two ways to register on the app.
- Create an [Email Session](https://appwrite.io/docs/client/account?sdk=web-default#accountCreateEmailSession)
- Using [Microsoft OAuth2](https://appwrite.io/docs/client/account?sdk=web-default#accountCreateOAuth2Session) login

Email is not verified unlike if the user changes his password.

User [preferences](https://appwrite.io/docs/client/account?sdk=web-default#accountUpdatePrefs) contain for the moment only the promo (Ex: 1l -> 1A Laval) key.

## Databases

There is currently one database named `Social Network` used for the real-time chat.

### e-pouet Collection

This collection contains all the pouet created by users.

| Name    | ID                     | Document Security |
|---------|------------------------|-------------------|
| e-pouet | `649a9c939205d302adf7` | -                 |

##### Permissions

| Role  | Create | Read | Update | Delete |
|-------|--------|------|--------|--------|
| Users | ✔      | ✔    | ❌      | ❌      |


##### Attributes

| Key             | Required | Type     | Default Value | Description                     |
|-----------------|----------|----------|---------------|---------------------------------|
| user_id         | ✔        | string   | -             | The UID of the pouet emitter    |
| user_name       | ✔        | string   | -             | The user name                   |
| message         | ✔        | string   | -             | The pouet message               |
| warning_message |          | string   | -             | The warning message if created  |
| poll            |          | string   | -             | The poll JS object if created   |
| images_id       |          | string[] | -             | The image Id pointing to bucket |

### likes Collection

This collection contains information about the pouet likes.


| Name  | ID                     | Document Security |
|-------|------------------------|-------------------|
| likes | `649a9eeb712ced5b5d9f` | -                 |

##### Permissions

| Role  | Create | Read | Update | Delete |
|-------|--------|------|--------|--------|
| Users | ❌      | ✔    | ❌      | ❌      |


##### Attributes

| Key      | Required | Type     | Default Value | Description               |
|----------|----------|----------|---------------|---------------------------|
| users_id | ✔        | string[] | -             | The UIDs who like a pouet |

Documents are identified by the e-pouet id.

### votes Collection

This collection contains information about the pouet likes.

| Name  | ID                     | Document Security |
|-------|------------------------|-------------------|
| votes | `64a8235f76db5003691f` | -                 |

##### Permissions

| Role  | Create | Read | Update | Delete |
|-------|--------|------|--------|--------|
| Users | ❌      | ✔    | ❌      | ❌      |

##### Attributes

| Key   | Required | Type     | Default Value | Description                             |
|-------|----------|----------|---------------|-----------------------------------------|
| users |          | string[] | -             | The UIDs who voted                      |
| value |          | string   | -             | The JS object of the current poll state |

Documents are identified by the e-pouet id.

## Functions

Functions are very useful, it allows executing custom code.
Function code can be edited in the `appwrite/functions` from the repository root folder.

### Likes

This function adds/removes a like to a pouet.

| Execute Access | API Key | Timeout | env    | Function ID            |
|----------------|---------|---------|--------|------------------------|
| Auth. Users    | ✔       | 5s      | NodeJS | `64b8e3372f1abee57485` |


#### Payload

| Key     | Required | Type   | Default Value | Description                             |
|---------|----------|--------|---------------|-----------------------------------------|
| pouetId | ✔        | string | -             | The pouetId where to add/remove a like. |

### Update Vote

Update a poll value.

| Execute Access | API Key | Timeout | env    | Function ID            |
|----------------|---------|---------|--------|------------------------|
| Auth. Users    | ✔       | 5s      | NodeJS | `64a82969485ea6e5e5df` |

#### Payload

| Key     | Required | Type   | Default Value | Description                         |
|---------|----------|--------|---------------|-------------------------------------|
| pouetId | ✔        | string | -             | The pouetId where to add an answer. |
| value   | ✔        | string | -             | The answer value                    |

### Delete Account

Delete an account and remove all pouets write by it.

| Execute Access | API Key | Timeout | env    | Function ID            |
|----------------|---------|---------|--------|------------------------|
| Auth. Users    | ✔       | 5s      | NodeJS | `64b3b3111d7a42051c6d` |

#### Payload

| Key      | Required | Type   | Default Value | Description                                                                                   |
|----------|----------|--------|---------------|-----------------------------------------------------------------------------------------------|
| userId   | ✔        | string | -             | UID                                                                                           |
| password | ✔        | string | -             | The password of the user. (Only [argon2](https://appwrite.io/docs/server/users) is supported) |


## Storage

Storage allows saving file to Appwrite easily.

### Avatars Bucket

Avatars are manager here.

| File Security | Encryption | Antivirus | Compression Algorithm | Maximum File size | Allowed File Extensions  |
|---------------|------------|-----------|-----------------------|-------------------|--------------------------|
| ✔             | ✔          | ✔         | Gzip                  | 2 Megabytes       | JPEG, JPG, PNG, GIF, SVG |

### E-Pouet

Stock pouets images.

| File Security | Encryption | Antivirus | Compression Algorithm | Maximum File size | Allowed File Extensions |
|---------------|------------|-----------|-----------------------|-------------------|-------------------------|
| ❌             | ✔          | ✔         | Gzip                  | 3 Megabytes       | JPG, PNG, SVG           |

## Support

If you need assistance or want to ask a question about the Android app, you are welcome to ask for support in our
Forums.
If you have found a bug, feel free to open a new Issue on Gitlab.

## Licence

The app core is under GPLv3 but be careful the Images are limited are restricted to
copyright.
